import { Server, Socket } from 'net';
import { Log } from './util/log';
import { config } from './config';
import { Packet, IPacketData } from './encapsulation';
import { IConfig } from './config';
import { buildSetTime } from './message-util';

export interface IConnection {
    ip: string;
    id: number;
    socket: Socket;
}

export interface IPacketCallbackArgs extends IPacketData {
    ip: String;
    statusCode?: number;
    status?: string;
    [key: string]: any;
}

export interface IHostOptions {
    config: IConfig;
}

type requestCallback = (request: IPacketCallbackArgs) => void;
type responseCallback = (response: IPacketCallbackArgs) => void;

// Host class definition (Singleton)
// -> used to control the host connection
//    for devices in the field (ie cranes)
export class Host {
    // Define private and static variables
    private static instance: Host;
    private server: Server;
    private log: Log;
    private config: IConfig;
    private requestHandlers: { [key: number]: requestCallback[] };
    private responseHandlers: { [key: number]: responseCallback[] };

    // Define public variables
    public id: number;
    public connIds: { [key: number]: string };
    private connections: { [key: string]: IConnection };

    // Define constructor for new instances
    private constructor(server: Server) {
        this.server = server;
        this.log = Log.getInstance();
        this.config = config;
        this.id = config.id;
        this.connections = {};
        this.connIds = {};
        this.requestHandlers = {};
        this.requestHandlers[0] = [];
        this.responseHandlers = {};
        this.responseHandlers[0] = [];

        this._initializeServerListeners();
    }

    static getInstance(server?: Server) {
        if (!Host.instance) {
            if (!server) {
                throw new Error(`Arguments required when initializing Host singleton`);
            }

            // Initialize Singleton
            Host.instance = new Host(server);
        }

        return Host.instance;
    }

    /* Public Method Definitions */

    public getConnectionByID(id: number): IConnection | null {
        const ip = this.connIds[id] ? this.connIds[id] : null;

        return ip ? this.connections[ip] : null;
    }

    // Handle new onResponse middleware
    public useRequest(cb: responseCallback) {
        this.requestHandlers[0].push(cb);
    }

    // Handle new onRequest subscriptions
    public onRequest(reqId: number, cb: requestCallback): void {
        if (reqId <= 0) throw new Error(`Error: Request ID must be greater than 0 but got ${reqId}`);
        if (!this.requestHandlers[reqId]) this.requestHandlers[reqId] = [];

        this.requestHandlers[reqId].push(cb);
    }

    // Handle new onResponse middleware
    public useResponse(cb: responseCallback) {
        this.responseHandlers[0].push(cb);
    }

    // Handle new onResponse subscriptions
    public onResponse(reqId: number, cb: responseCallback): void {
        if (reqId <= 0) throw new Error(`Error: Request ID must be greater than 0 but got ${reqId}`);
        if (!this.responseHandlers[reqId]) this.responseHandlers[reqId] = [];

        this.responseHandlers[reqId].push(cb);
    }

    // Send packet to client with passed destination id
    public sendRequest(buffer: Buffer, destId: number, cb?: (err?: Error) => void) {
        const clientIp = destId in this.connIds ? this.connIds[destId] : null;

        if (clientIp) {
            const client = this.connections[clientIp];
            const packet = Packet.build(buffer, { destId, sourceId: this.id });

            if (cb) client.socket.write(packet, cb);
            else client.socket.write(packet);
        } else {
            this.log.error(`No client with ip ${clientIp} has connected to the server`);
        }
    }

    /* Private Method Definitions */

    // Initialize server listeners
    private _initializeServerListeners() {
        // Handle new connections
        this.server.on('connection', socket => {
            const { remoteAddress: ip } = socket;
            this.log.client(`${ip} has initiated connection`);

            this._initializeNewConnection(socket);
        });
    }

    // Initialize client listeners
    private _initializeNewConnection(socket: Socket) {
        const { remoteAddress: ip } = socket;

        // Handle a closed client connection
        socket.on('close', () => {
            ip && this._dropConnection(ip);

            this.log.client(`${ip} connection terminated`);
        });

        // Handle error on socket
        socket.on('error', () => {
            this.log.error(`${ip} connection has errored`);
        });

        // Get registered client from config data if available
        const client = this._getClientDataConfig(socket);

        // If client exists then use it
        if (client) {
            // Add client to connection dictionaries for lookup
            this.connections[client.ip] = client;
            this.connIds[client.id] = client.ip;

            // Send time sync command to client on connection
            this.log.server(`${ip} sending set time request`);
            this.sendRequest(buildSetTime(new Date()), client.id);

            // Handle data event from client
            socket.on('data', data => {
                // Validate proper packet format before parsing
                if (Packet.isValidPacket(data)) {
                    // Parse packet and extract request id
                    const parsed = Packet.parse(data);
                    const reqId = parsed.data.readInt32LE(0);

                    // Handle request packets from client
                    if (reqId < 10000 && this.requestHandlers[reqId]) {
                        const middleware = this.requestHandlers[0];
                        const handlers = this.requestHandlers[reqId];
                        const request = { ...parsed, reqId, ip: ip || '' };

                        for (const fn of middleware) {
                            fn(request);
                        }

                        for (const handler of handlers) {
                            handler(request);
                        }
                    }

                    // Handle response packet from client
                    if (reqId >= 10000 && this.responseHandlers[reqId - 100000]) {
                        const middleware = this.responseHandlers[0];
                        const handlers = this.responseHandlers[reqId - 100000];
                        const request = { ...parsed, reqId: reqId - 100000, ip: ip || '' };

                        for (const fn of middleware) {
                            fn(request);
                        }

                        for (const handler of handlers) {
                            handler(request);
                        }
                    }
                } else {
                    this.log.error(`${ip} sent invalid common message protocol packet`);
                }
            });
        } else {
            // If client is unregistered, then
            //   destroy the socket immediately
            socket.destroy();
        }
    }

    // Remove connection from active connection
    //   dictionaries if exists
    private _dropConnection(ip: string) {
        const client = this.connections[ip];

        if (client) {
            delete this.connections[ip];
            delete this.connIds[client.id];
        }
    }

    // Fetch client data from config if available
    private _getClientDataConfig(socket: Socket): IConnection | null {
        const { remoteAddress: ip } = socket;

        const { cranes } = this.config;
        const filtered = cranes.filter(crane => crane.ip === ip);
        const crane = filtered.length > 0 ? filtered[0] : null;

        return crane ? { ...crane, socket } : null;
    }
}
