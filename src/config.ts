export interface IClientInterface {
    ip: string;
    id: number;
}

export interface IConfig {
    id: number;
    cranes: [IClientInterface];
}

export const config: IConfig = {
    id: 10000,
    cranes: [
        {
            ip: '192.168.1.1',
            id: 10001,
        },
    ],
};
