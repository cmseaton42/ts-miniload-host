import { createServer } from 'net';
import { Log } from './util/log';
import { connect } from 'mongoose';
import { env } from './env';
import { Host } from './host';
import { initializeHostRouter } from './host-router';
import bodyParser from 'body-parser';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { initializeApi } from './api';
import { typeDefs, resolvers } from './graphql';

// Initialize useful constants
const PORT = 3000;
const API_PORT = 8080;
const IP_ADDR = '0.0.0.0';

// Create logger instance
const log = Log.getInstance();

// Create server
const server = createServer();

// Initialize Host controller singleton
Host.getInstance(server);

// Initialize MongoDB connection
connect(
    env.MONGODB_URI,
    {
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
        useUnifiedTopology: true,
    },
)
    .then(() => {
        log.info('Connection to mongodb has been established');
    })
    .catch((err: Error) => {
        log.error('Failed to connect to db');
        process.exit(1);
    });

// Initialize host message router
initializeHostRouter();

// Begin listening for connections
server.listen(PORT, IP_ADDR, () => {
    log.server(` Host controller listening on Port ${PORT}...`);
});

// Create web api for handling requests
const app = express();

// Initialize GraphQL endpoint
const apollo = new ApolloServer({ typeDefs, resolvers });
apollo.applyMiddleware({ app });

// Register useful middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Register Web API request handlers
// initializeApi(app);

// Start API endpoint
app.listen(API_PORT, IP_ADDR, () => {
    log.server(`Web API listening on Port ${API_PORT}...`);
});
