import winston, { createLogger, format, transports } from 'winston';
import Transport from 'winston-transport';
import { Log as Entry } from '../models';

// Custom MongoDB winston log transport
class MongoTransport extends Transport {
    constructor() {
        super();
    }

    log(info: any, callback: () => undefined) {
        setImmediate(() => this.emit('logged', info));

        const accessors = Object.getOwnPropertySymbols(info);
        const level = info[accessors[0]];
        const msg = info['message'];

        const entry = new Entry();
        entry.message = msg;
        entry.level = level;

        entry.save();

        if (callback) callback();
    }
}

// Logger Singleton Class
export class Log {
    private static instance: Log;
    private logger: winston.Logger;

    private constructor() {
        // Create logger instance
        this.logger = createLogger({
            level: 'Debug',
            format: format.combine(format.colorize(), format.simple()),
            transports: [new transports.Console(), new MongoTransport()],
            levels: {
                Error: 0,
                Warning: 1,
                Server: 2,
                Client: 3,
                Info: 4,
                Debug: 5,
            },
        });

        // Register console log colors
        winston.addColors({
            Error: 'bold red',
            Warning: 'bold orange',
            Server: 'bold blue',
            Client: 'bold magenta',
            Info: 'bold yellow',
            Debug: 'bold yellow',
        });
    }

    private log(msg: string, type: string) {
        this.logger.log(type, msg);
    }

    error(msg: string) {
        this.log(msg, 'Error');
    }

    warning(msg: string) {
        this.log(msg, 'Warning');
    }

    server(msg: string) {
        this.log(msg, 'Server');
    }

    client(msg: string) {
        this.log(msg, 'Client');
    }

    info(msg: string) {
        this.log(msg, 'Info');
    }

    debug(msg: string) {
        this.log(msg, 'Debug');
    }

    static getInstance() {
        // Create logger instance
        if (!Log.instance) {
            Log.instance = new Log();
        }

        return Log.instance;
    }
}
