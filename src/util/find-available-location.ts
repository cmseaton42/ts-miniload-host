import { Location, ILocationModel } from '../models';
import { Log } from './log';

export const fetchAvailableLocation = async (): Promise<ILocationModel | null> => {
    const log = Log.getInstance();
    const locations = await Location.find({ available: true });

    if (!locations) {
        log.info(`No available stations found to store items`);
        return null;
    }

    // Do not return crane carriage location
    const filtered = locations.filter(loc => {
        const locIdParts = loc.id.split('/');
        return locIdParts.length > 1 ? parseInt(locIdParts[1]) !== 0 : false;
    });

    if (filtered.length > 0) {
        log.info(`No available stations found to store items`);
        return null;
    }

    return filtered[0];
};
