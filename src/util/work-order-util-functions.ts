import { IWorkOrderModel, IItemModel, ILocationModel, WorkOrder, Item, Location, workOrderStatus } from '../models';
import { Log } from './log';
import { fetchAvailableLocation } from './find-available-location';
import { buildWorkOrderRequest } from '../message-util';
import { Host } from '../host';
import uuid from 'uuid/v4';

export interface IWorkOrderAssets {
    order: IWorkOrderModel;
    item: IItemModel;
    crane: ILocationModel;
    origin?: ILocationModel;
    destination?: ILocationModel;
    isPlace: boolean;
    isPick: boolean;
    isPickAndPlace: boolean;
}

export interface IFetchOrderAssets {
    error: boolean;
    assets?: IWorkOrderAssets;
}

// Get logger singleton
const log = Log.getInstance();

// Fetch next work order to be processed
export const fetchNextWorkOrder = async (arg: number | ILocationModel): Promise<IWorkOrderModel | null> => {
    // Fetch crane if id was passed
    const crane = typeof arg === 'number' ? await Location.findOne({ id: `${arg}/0` }) : arg;
    if (!crane) return null;

    // Determine if should only send place work orders
    const shouldPlace = crane.available;

    // Fetch possible work orders
    const searchOpts = { status: workOrderStatus.UNSENT, crane: crane.id };
    const unfiltered = await WorkOrder.find(searchOpts).sort('timestamp');
    const orders = shouldPlace ? unfiltered.filter(order => order.origin === null) : unfiltered;

    if (!orders || orders.length === 0) return null;
    return orders[0];
};

// Fetch all relavant work order data assets from mongo
export const fetchWorkOrderDataAssets = async (id: string): Promise<IFetchOrderAssets> => {
    // Lookup order
    const order = await WorkOrder.findOne({ id });

    // Verify order exists
    if (!order) {
        log.error(`work order: ${id} could not be found`);
        return { error: true };
    }

    // Lookup item, origin, and destination
    const item = await Item.findOne({ id: order.item });

    if (!item) {
        log.error(`item id: ${order.item} could not be found`);
        return { error: true };
    }

    // Lookup origin, and destination
    const origin = await Location.findOne({ id: order.origin });
    const destination = await Location.findOne({ id: order.destination });

    if (!origin && !destination) {
        log.error(`work order ${id} must have either a destination or a origin`);
        return { error: true };
    }

    const craneId = origin ? origin.crane : destination ? destination.crane : null;
    const isPickAndPlace = origin && destination ? true : false;
    const isPick = origin && !destination ? true : false;
    const isPlace = !origin && destination ? true : false;

    // Lookup Crane Location
    const crane = await Location.findOne({ id: `${craneId}/0` });

    if (!crane) {
        log.error(`location (crane carriage) ${craneId}/0 does not exist`);
        return { error: true };
    }

    const assets = {
        order,
        item,
        origin: origin || undefined,
        destination: destination || undefined,
        crane,
        isPick,
        isPlace,
        isPickAndPlace,
    };

    return { error: false, assets };
};

// Send next work order to crane
export const sendNextWorkOrderToCrane = async (arg: number | ILocationModel): Promise<undefined> => {
    // Fetch next work order
    const order = await fetchNextWorkOrder(arg);
    if (!order) return;

    // Fetch work order data assets
    const { error, assets } = await fetchWorkOrderDataAssets(order.id);
    if (error || !assets) return;

    // Prep to send work order to crane to be processed
    log.server(`Sending work order id: ${order.id} to crane id: ${assets.crane.id}`);

    const workOrderPayload = buildWorkOrderRequest({
        id: order.id,
        to: assets.destination ? assets.destination.station : undefined,
        from: assets.origin ? assets.origin.station : undefined,
    });

    // Send work order
    const host = Host.getInstance();
    host.sendRequest(workOrderPayload, assets.crane.crane);

    return;
};

export const handleUpdateComplete = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const { item, crane, order, origin, destination } = assets;

    if (origin) {
        // Update Origin
        origin.available = true;
        origin.item = null;
        origin.lastUpdated = now;

        // Update Item
        item.location = crane.id;
        item.lastUpdated = now;

        // Update Crane Location
        crane.item = order.item;
        crane.lastUpdated = now;
    }

    if (destination) {
        // Update Destination
        destination.available = false;
        destination.item = order.item;
        destination.lastUpdated = now;

        // Update Item
        item.location = destination.id;
        item.lastUpdated = now;

        // Update Crane Location
        crane.item = null;
        crane.lastUpdated = now;
    }

    // Save Updates to DB
    await item.save();
    await crane.save();
    origin && (await origin.save());
    destination && (await destination.save());

    // Attempt to send new work order to idle crane
    await sendNextWorkOrderToCrane(crane);

    return;
};

export const handleUpdateProcessing = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const { item, crane, order, origin } = assets;

    if (origin) {
        // Update Origin
        origin.available = true;
        origin.item = null;
        origin.lastUpdated = now;

        // Update Item
        item.location = crane.id;
        item.lastUpdated = now;

        // Update Crane Location
        crane.item = order.item;
        crane.lastUpdated = now;
    }

    origin && (await origin.save());
    await order.save();
    await crane.save();

    return;
};

export const handleUpdateNoItemToPick = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const { item, origin } = assets;

    log.info(`Clearing location for item with id: ${item.id}`);
    item.location = null; // Unknown
    item.lastUpdated = now;

    if (origin) {
        log.info(`Clearing item from location with id: ${origin.id}`);
        origin.item = null;
        origin.available = true;
        origin.lastUpdated = now;
        await origin.save();
    }

    await item.save();
    return;
};

export const handleUpdateCarriageNotEmpty = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const host = Host.getInstance();
    const { order, crane } = assets;

    log.info(`Setting work order ${order.id} status to UNSENT to be processed later`);

    order.status = workOrderStatus.UNSENT;
    order.lastUpdated = now;
    await order.save();

    log.info('Generating new item id for unknown item on carriage');

    // Generate item entry for item on crane
    const newItem = new Item();
    newItem.id = uuid();
    newItem.location = crane.id;
    newItem.barcode = 'UNKNOWN';

    log.info(`Adding new item id: ${newItem.id} to crane id: ${crane.id}`);

    // Update crane, add item
    crane.available = false;
    crane.item = newItem.id;
    crane.lastUpdated = now;

    await crane.save();
    await newItem.save();

    const dest = await fetchAvailableLocation();
    if (!dest) return;

    log.info(`Generating new work order to put away (store) new item`);

    // Generate "Place" work order to store new item
    const newOrder = new WorkOrder();
    newOrder.id = uuid();
    newOrder.destination = dest.id;
    newOrder.crane = dest.crane;

    await newOrder.save();

    log.server(`Sending new put away work order to crane ${crane.crane}`);

    const workOrderPayload = buildWorkOrderRequest({
        id: newOrder.id,
        to: dest.station,
    });

    host.sendRequest(workOrderPayload, dest.crane);

    return;
};

export const handleUpdateNoItemToPlace = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const { item, crane } = assets;

    log.info(`Setting item Location ${item.id} to Unknown <NULL>`);

    // Update item
    item.location = null;
    item.lastUpdated = now;

    log.info(`Setting crane carriage ${crane.id} to available`);

    // Update crane
    crane.available = true;
    crane.lastUpdated = now;
    crane.item = null;

    await item.save();
    await crane.save();

    // Send next work order to crane
    sendNextWorkOrderToCrane(crane);

    return;
};

export const handleUpdateDestUnavailable = async (assets: IWorkOrderAssets): Promise<undefined> => {
    const now = Date.now();
    const { crane, destination } = assets;

    // Update crane
    crane.lastUpdated = now;
    await crane.save();

    if (destination) {
        // Generate new item as placeholder
        //    for item in destination
        const item = new Item();
        item.id = uuid();
        item.barcode = 'UNKNOWN';
        item.location = destination.id;

        // Update destination
        destination.item = item.id;
        destination.available = false;
        destination.lastUpdated = now;

        await item.save();
        await destination.save();
    }

    // Send next work order to crane
    sendNextWorkOrderToCrane(crane);

    return;
};
