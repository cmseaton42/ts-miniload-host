import { Schema, model, Document } from 'mongoose';
import uuid from 'uuid/v4';

export interface ILogModel extends Document {
    id: string;
    level: string;
    message: string;
    timestamp: number;
}

const logSchema = new Schema({
    id: { type: String, default: uuid, unique: true },
    level: String,
    message: String,
    timestamp: { type: Date, default: Date.now },
});

export const Log = model<ILogModel>('Log', logSchema);
