import { Schema, model, Document } from 'mongoose';
import uuid from 'uuid/v4';

export interface IItemModel extends Document {
    id: string;
    barcode: string;
    location: string | null;
    timestamp: number;
    lastUpdated: number;
}

const itemSchema = new Schema({
    id: { type: String, default: uuid, unique: true },
    barcode: String,
    location: String, // id of last known location
    timestamp: { type: Date, default: Date.now },
    lastUpdated: { type: Date, default: Date.now },
});

export const Item = model<IItemModel>('Item', itemSchema);
