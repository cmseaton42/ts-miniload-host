import { Schema, model, Document } from 'mongoose';

export interface ILocationModel extends Document {
    id: string;
    crane: number;
    station: number;
    available: boolean;
    item: string | null;
    lastUpdated: number;
}

const locationSchema = new Schema({
    id: { type: String, unique: true },
    crane: Number,
    station: Number,
    available: { type: Boolean, default: true },
    item: String, // id of stored item
    lastUpdated: { type: Date, default: Date.now },

    // Reserved for future use
    coordinate: {
        x: Number,
        y: Number,
        z: Number,
    },
});

export const Location = model<ILocationModel>('Location', locationSchema);
