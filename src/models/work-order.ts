import { Schema, model, Document } from 'mongoose';
import uuid from 'uuid/v4';

export enum workOrderStatus {
    UNSENT = 0,
    ACKNOWLEDGED = 1,
    PROCESSING = 10,
    COMPLETE = 100,

    // All statuses greater than
    //   200 are failure codes
    GENREAL_FAILURE = 200,
    NOT_IN_REMOTE_MODE = 201,
    NOT_READY_FOR_NEW_ORDER = 202,
    NO_ITEM_TO_PLACE = 210,
    DESTINATION_UNAVAILABLE = 211,
    CARRIAGE_NOT_EMPTY_TO_PICK = 220,
    PICK_LOCATION_EMPTY = 221,
    CRANE_UNVAILABLE = 250,
    NOT_REMOTE_IN_CYCLE = 251,
    TIMEOUT = 290,
}

export interface IWorkOrderModel extends Document {
    id: string;
    item: string;
    origin: string | null;
    destination: string | null;
    crane: number;
    status: number;
    timestamp: number;
    lastUpdated: number;
}

const workOrderSchema = new Schema({
    id: { type: String, default: uuid, unique: true },
    item: String, // id of stored item
    origin: String, // id of origin location
    destination: String, // id of destination location
    crane: Number,
    /*****************************************
        Order Statuses
        000 - Unsent
        010 - Processing
        100 - Complete / Success
        210 - No Item available to place
        211 - Destination unavailable/ already consumed
        220 - Item already on carriage / cannot pick
        221 - No Item available at pickup location
    */
    status: { type: Number, default: 0 },
    timestamp: { type: Date, default: Date.now },
    lastUpdated: { type: Date, default: Date.now },
});

export const WorkOrder = model<IWorkOrderModel>('WorkOrder', workOrderSchema);
