import { Host } from '../host';
import { Application } from 'express';
import { Logger } from 'winston';

export const initializeApi = (app: Application, host: Host, logger: Logger) => {
    app.get('/', (req, res) => {
        if (process.env.NODE_ENV && process.env.NODE_ENV !== 'Production') {
            res.send('Hello from the crane web server!');
        } else {
            // TODO: Finish for production version
        }
    });
};
