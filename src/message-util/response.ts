export interface IResponseData {
    id: number;
    statusCode: number;
    status: string;
}

export enum ResponseCodes {
    SUCCESS = 100,
    FAILURE = 200,
    BAD_REQUEST = 299,
}

export const parseResponse = (data: Buffer): IResponseData => {
    const size = data.readInt32LE(8);
    let msg: string = '';

    if (size > 0) {
        const msgBuf = Buffer.alloc(size);
        data.copy(msgBuf, 0, 12, 12 + size);

        msg = msgBuf.toString('utf8');
    }

    return {
        id: data.readInt32LE(0) - 100000,
        statusCode: data.readInt32LE(4),
        status: msg,
    };
};
