const COMMAND_PERFORM_WORK_ORDER = 1250;

export interface IPerformRequestOpts {
    id: string;
    from?: number;
    to?: number;
}

export interface IWorkOrderStatusData {
    id: string;
    statusCode: number;
    status: string;
}

export enum WorkOrderStatus {
    PROCESSING = 1,
    SUCCESS = 2,
    DEST_UNAVAILABLE = 3,
}

// Build perform work order request
export const buildWorkOrderRequest = (opts: IPerformRequestOpts): Buffer => {
    const buf = Buffer.alloc(60);

    // Must have at least a from or a to
    if (!opts.from && !opts.to)
        throw new Error('Error: Work order must have at least an origin or a destination but received neither');

    // Write request id
    buf.writeInt32LE(COMMAND_PERFORM_WORK_ORDER, 0);

    // Write work order id
    if (opts.id.length !== 36) throw new Error('Error: Work order id must be 36 characters long');
    const id = Buffer.from(opts.id);

    id.copy(buf, 5, 0);
    buf.writeInt8(opts.id.length, 4);

    // Handle from (origin) if available
    if (opts.from) {
        buf.writeInt32LE(opts.from, 45);
    }

    // Handle to (destination) if available
    if (opts.to) {
        buf.writeInt32LE(opts.to, 53);
    }

    return buf;
};

// Parse work order status request
export const parseWorkOrderStatus = (data: Buffer): IWorkOrderStatusData => {
    const size = data.readInt8(4);
    const idBuf = Buffer.alloc(size);
    const statusCode = data.readInt32LE(41);
    const status = WorkOrderStatus[statusCode] || '';

    data.copy(idBuf, 0, 5, 5 + size);

    const id = idBuf.toString('utf8');

    return { id, statusCode, status };
};
