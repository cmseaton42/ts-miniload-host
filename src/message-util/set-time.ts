const COMMAND_SET_TIME = 1000;

export const buildSetTime = (date: Date): Buffer => {
    const buf = Buffer.alloc(32);

    // Build buffer
    buf.writeInt32LE(COMMAND_SET_TIME, 0);
    buf.writeInt32LE(date.getFullYear(), 4);
    buf.writeInt32LE(date.getMonth() + 1, 8);
    buf.writeInt32LE(date.getDate(), 12);
    buf.writeInt32LE(date.getHours(), 16);
    buf.writeInt32LE(date.getMinutes(), 20);
    buf.writeInt32LE(date.getSeconds(), 24);
    buf.writeInt32LE(date.getMilliseconds() * 1000, 28);

    return buf;
};
