import { ILocationModel } from '../../../models';

export interface ILocation {
    id: String;
    crane: Number;
    station: Number;
    available: Boolean;
    lastUpdated: Number;
    item: String | null;
}

export const buildLocationFromDB = (location: ILocationModel): ILocation => {
    return {
        id: location.id,
        crane: location.crane,
        station: location.station,
        available: location.available,
        lastUpdated: location.lastUpdated,
        item: location.item,
    };
};
