import { Item } from '../../../models';
import { buildItemFromDB } from '../item/helper';

interface IItemLocationArgs {
    item: String;
}

const item = async (par: IItemLocationArgs) => {
    const item = await Item.findOne({ id: par.item });

    return item ? buildItemFromDB(item) : null;
};

export const Location = {
    item,
};
