import { Location } from '../../../models';
import { buildLocationFromDB } from './helper';

interface ICreateLocationArgs {
    crane: number;
    station: number;
}

const createLocation = async (_: any, args: ICreateLocationArgs) => {
    const { crane, station } = args;

    const newLocation = new Location();
    newLocation.id = `${crane}/${station}`;
    newLocation.crane = crane;
    newLocation.station = station;

    const location = await newLocation.save();

    return buildLocationFromDB(location);
};

export const Mutation = {
    CreateLocation: createLocation,
};
