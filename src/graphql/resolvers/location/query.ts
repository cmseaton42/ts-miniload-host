import { Location } from '../../../models';
import { buildLocationFromDB } from './helper';

interface ILocationFilterArgs {
    empty: Boolean;
}

const fetchLocations = async (_: any, args: ILocationFilterArgs) => {
    const opts = args.empty !== undefined ? { available: args.empty } : {};

    const locations = await Location.find(opts);

    return locations.map(location => buildLocationFromDB(location));
};

export const Query = {
    Locations: fetchLocations,
};
