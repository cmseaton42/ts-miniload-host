import { IWorkOrderModel } from '../../../models';

export interface IWorkOrder {
    id: String;
    item: String;
    origin: String | null;
    destination: String | null;
    status: Number;
    timestamp: Number;
    lastUpdated: Number;
}

export const buildWorkOrderFromDB = (workOrder: IWorkOrderModel): IWorkOrder => {
    return {
        id: workOrder.id,
        item: workOrder.item,
        origin: workOrder.origin,
        destination: workOrder.destination,
        status: workOrder.status,
        timestamp: workOrder.timestamp,
        lastUpdated: workOrder.lastUpdated,
    };
};
