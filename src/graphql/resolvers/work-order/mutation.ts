import { sendNextWorkOrderToCrane } from '../../../util/work-order-util-functions';
import { WorkOrder, workOrderStatus, Item, Location } from '../../../models';
import { buildWorkOrderFromDB } from './helper';

interface ICreateWorkOrderArgs {
    args: {
        item: string;
        origin: string;
        destination: string;
    };
}

interface IDeleteWorkOrderArgs {
    id: string;
}

const createWorkOrder = async (_: any, { args }: ICreateWorkOrderArgs) => {
    const item = await Item.findOne({ id: args.item });
    if (!item) throw new Error(`Could not find item with ID: ${args.item}`);

    const origin = args.origin ? await Location.findOne({ id: args.origin }) : null;
    const destination = args.destination ? await Location.findOne({ id: args.destination }) : null;

    if (!origin && !destination) {
        throw new Error('Work orders must either valid origin or valid destination');
    }

    if (origin && origin.id !== item.location) {
        throw new Error(
            `Item with ID: ${item.id} is not registered to ${origin.id} and is currently registered to ${item.location}`,
        );
    } else if (!origin && destination) {
        const crane = await Location.findOne({ id: `${destination.crane}/0` });

        if (!crane) throw new Error(`Could not find crane carriage location ${destination.crane}/0`);

        if (crane.id !== item.location) {
            throw new Error(
                `Item with ID: ${item.id} is not registered to ${crane.id}/0 and is currently registered to ${item.location}`,
            );
        }
    }

    const newWorkOrder = new WorkOrder();
    newWorkOrder.item = args.item;
    newWorkOrder.origin = origin ? origin.id : null;
    newWorkOrder.destination = destination ? destination.id : null;
    newWorkOrder.status = workOrderStatus.UNSENT;
    newWorkOrder.crane = origin ? origin.crane : destination ? destination.crane : 99999;

    const workOrder = await newWorkOrder.save();

    if (!workOrder) {
        throw new Error('Failed to create new work order');
    }

    await sendNextWorkOrderToCrane(newWorkOrder.crane);

    return buildWorkOrderFromDB(workOrder);
};

const deleteWorkOrder = async (_: any, args: IDeleteWorkOrderArgs) => {
    const workOrder = WorkOrder.findOneAndDelete({ id: args.id });

    if (!workOrder) throw new Error(`Could not locate work order with ID: ${args.id}`);

    return workOrder;
};

export const Mutation = {
    CreateWorkOrder: createWorkOrder,
    DeleteWorkOrder: deleteWorkOrder,
};
