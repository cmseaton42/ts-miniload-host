import { WorkOrder } from '../../../models';
import { buildWorkOrderFromDB } from './helper';

const fetchWorkOrders = async () => {
    const WorkOrders = await WorkOrder.find({});

    return WorkOrders.map(workOrder => buildWorkOrderFromDB(workOrder));
};

export const Query = {
    WorkOrders: fetchWorkOrders,
};
