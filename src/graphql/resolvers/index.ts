import * as itemResolver from './item';
import * as locationResolver from './location';
import * as workOrderResolver from './work-order';

export const resolvers = [itemResolver, locationResolver, workOrderResolver];
