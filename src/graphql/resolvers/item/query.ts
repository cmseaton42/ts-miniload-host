import { Item } from '../../../models';
import { buildItemFromDB } from './helper';

const fetchItems = async () => {
    const items = await Item.find({});

    return items.map(item => buildItemFromDB(item));
};

export const Query = {
    Items: fetchItems,
};
