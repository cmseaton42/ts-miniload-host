import { Location } from '../../../models';
import { buildLocationFromDB } from '../location/helper';

interface ILocationItemArgs {
    location: String;
}

const location = async (par: ILocationItemArgs) => {
    const location = await Location.findOne({ id: par.location });

    return location ? buildLocationFromDB(location) : null;
};

export const Item = {
    location,
};
