import { IItemModel } from '../../../models';

export interface IItem {
    id: String;
    barcode: String;
    location: String | null;
    timestamp: Number;
    lastUpdated: Number;
}

export const buildItemFromDB = (item: IItemModel): IItem => {
    return {
        id: item.id,
        barcode: item.barcode,
        location: item.location,
        timestamp: item.timestamp,
        lastUpdated: item.lastUpdated,
    };
};
