import { Item, Location } from '../../../models';
import { buildItemFromDB } from './helper';
import uuid from 'uuid/v4';

interface ICreateItemArgs {
    barcode: string;
    location: string | null;
}

interface IDeleteItemArgs {
    id: string;
}

const createItem = async (_: any, args: ICreateItemArgs) => {
    const barcode = args.barcode;
    const locId = args.location || null;
    const id = uuid();

    if (locId) {
        const location = await Location.findOne({ id: locId });
        if (!location) throw new Error(`Could not find a valid location with ID: ${locId}`);

        location.item = id;
        location.available = false;

        await location.save();
    }

    const newItem = new Item();
    newItem.id = id;
    newItem.barcode = barcode;
    newItem.location = locId;

    const item = await newItem.save();

    return buildItemFromDB(item);
};

const deleteItem = async (_: any, args: IDeleteItemArgs) => {
    const { id } = args;

    const location = await Location.findOne({ item: id });

    if (location) {
        location.item = null;
        await location.save();
    }

    const item = await Item.findOneAndDelete({ id });

    if (!item) throw new Error(`Could not find item with ID: ${id}`);

    return buildItemFromDB(item);
};

export const Mutation = {
    CreateItem: createItem,
    DeleteItem: deleteItem,
};
