import { itemType } from './item';
import { locationType } from './location';
import { workOrderType } from './work-order';

export const types = [itemType, locationType, workOrderType];
