import { gql } from 'apollo-server-express';

export const locationType = gql`
    type Location {
        id: ID!
        crane: Int!
        station: Int!
        available: Boolean!
        item: Item
        lastUpdated: Int!
    }
`;
