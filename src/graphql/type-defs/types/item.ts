import { gql } from 'apollo-server-express';

export const itemType = gql`
    type Item {
        id: ID!
        barcode: String
        location: Location
        timestamp: Int!
        lastUpdated: Int!
    }
`;
