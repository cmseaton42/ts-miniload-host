import { gql } from 'apollo-server-express';

export const workOrderType = gql`
    input CreateWorkOrderArgs {
        item: ID!
        origin: ID
        destination: ID
    }

    input UpdateWorkOrderArgs {
        origin: ID
        destination: ID
        status: Int
    }

    type WorkOrder {
        id: ID!
        item: Item
        origin: Location
        destination: Location
        status: Int
        timestamp: Int!
        lastUpdated: Int!
    }
`;
