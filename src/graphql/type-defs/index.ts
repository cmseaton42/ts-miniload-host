import { types } from './types';
import { query } from './query';
import { mutation } from './mutation';

export const typeDefs = [...types, query, mutation];
