import { gql } from 'apollo-server-express';

export const mutation = gql`
    type Mutation {
        # --- Item/ Tote Queries ---
        CreateItem(barcode: String!, location: ID): Item!
        # UpdateLocation(id: ID!, location: ID!): Item!
        DeleteItem(id: ID!): Item!

        # --- Location Queries ---
        CreateLocation(crane: Int!, station: Int!): Location!
        # DeleteLocation(id: ID!): Location!
        # EmptyLocation(id: ID!): Location!

        # --- Work Order Queries ---
        CreateWorkOrder(args: CreateWorkOrderArgs): WorkOrder!
        # UpdateWorkOrder(args: UpdateWorkOrderArgs): WorkOrder!
        DeleteWorkOrder(id: ID!): WorkOrder!
    }
`;
