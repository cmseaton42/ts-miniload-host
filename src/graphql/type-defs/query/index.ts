import { gql } from 'apollo-server-express';

export const query = gql`
    type Query {
        # Item/ Tote Queries
        Items: [Item]!

        # Location Queries
        Locations(empty: Boolean): [Location]!

        # Work Order Queries
        WorkOrders: [WorkOrder]!
    }
`;
