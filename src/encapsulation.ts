export interface IPacketBuildOptions {
    sourceId: number;
    destId: number;
}

export interface IPacketData extends IPacketBuildOptions {
    data: Buffer;
}

const ASCII_STX = 0x02;
const ASCII_EOT = 0x04;

export abstract class Packet {
    // Build common protocol buffer
    static build(data: Buffer, opts: IPacketBuildOptions): Buffer {
        // Allocate a buffer with appropriate
        //   space to encapsulate data
        const buf = Buffer.alloc(data.length + 12);

        // Populate packet overhead
        buf.writeInt8(ASCII_STX, 0);
        buf.writeInt16LE(data.length, 1);
        buf.writeInt32LE(opts.sourceId, 3);
        buf.writeInt32LE(opts.destId, 7);
        buf.writeInt8(ASCII_EOT, 11 + data.length);

        // Embed data payload into packet
        data.copy(buf, 11, 0);

        return buf;
    }

    // Validate incoming common protocol message
    static isValidPacket(buffer: Buffer): Boolean {
        const size = buffer.readInt16LE(1);
        const hasSTX = buffer.readInt8(0) === ASCII_STX;
        const hasEOT = buffer.readInt8(size + 11) === ASCII_EOT;
        const sizeIsOk = size >= 4;
        const isValidId = buffer.readInt32LE(3) > 0 && buffer.readInt32LE(7) > 0;
        const isValidReq = buffer.readInt32LE(11) > 0;

        return hasSTX && hasEOT && sizeIsOk && isValidId && isValidReq;
    }

    // Parse incoming common protocol message
    static parse(buffer: Buffer): IPacketData {
        // Extract payload data size
        const size = buffer.readInt16LE(1);

        // Return decoded data
        return {
            sourceId: buffer.readInt32LE(3),
            destId: buffer.readInt32LE(7),
            data: buffer.slice(11, 11 + size),
        };
    }
}
