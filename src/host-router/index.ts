import { initSetTimeRouter } from './set-time';
import { initHostMiddleware } from './middleware';
import { initWorkOrderRouter } from './work-order';

export enum Requests {
    SET_TIME = 1000,
    CHANGE_MODE = 1100,
    MOVE_AXIS = 1200,
    WORK_ORDER = 1250,
    WORK_ORDER_STATUS_UPDATE = 2250,
}

export const initializeHostRouter = () => {
    initHostMiddleware();
    initWorkOrderRouter();
    initSetTimeRouter();
};
