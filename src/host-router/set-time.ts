import { Host } from '../host';
import { Log } from '../util/log';
import { Requests } from './index';

export const initSetTimeRouter = () => {
    // Get useful singletons
    const host = Host.getInstance();
    const log = Log.getInstance();

    host.onResponse(Requests.SET_TIME, response => {
        log.client(`${response.ip} has synced time with host`);
    });
};
