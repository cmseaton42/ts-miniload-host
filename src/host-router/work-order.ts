import { Host } from '../host';
import { Log } from '../util/log';
import { Requests } from './index';
import { workOrderStatus } from '../models';
import { parseWorkOrderStatus } from '../message-util/work-order';
import { WorkOrder } from '../models';

import {
    fetchWorkOrderDataAssets,
    handleUpdateComplete,
    handleUpdateProcessing,
    handleUpdateNoItemToPick,
    handleUpdateCarriageNotEmpty,
    handleUpdateNoItemToPlace,
    handleUpdateDestUnavailable,
} from '../util/work-order-util-functions';

export const initWorkOrderRouter = () => {
    // Get useful singletons
    const host = Host.getInstance();
    const log = Log.getInstance();

    // Handle work order response
    host.onResponse(Requests.WORK_ORDER, async response => {
        const status = response.data.readInt32LE(4);
        const size = response.data.readInt32LE(8);
        const id = response.data.slice(12, 48).toString('utf8');
        const msg = size > 36 ? response.data.slice(48).toString('utf8') : '';

        log.client(`${response.sourceId} acknowledged work order ${id}`);

        if (status >= workOrderStatus.GENREAL_FAILURE) {
            log.error(`Work order ${id} rejected -> ${msg}`);

            switch (status) {
                case workOrderStatus.NOT_IN_REMOTE_MODE:
                    log.info(`Order ${id} waiting for crane mode change`);
                    break;
                case workOrderStatus.NOT_READY_FOR_NEW_ORDER:
                    log.info(`Order ${id} waiting on crane to become available`);
                    break;
                default:
                    log.info(`Removing work order ${id}`);
                    await WorkOrder.findOneAndDelete({ id });
                    break;
            }
        } else {
            const order = await WorkOrder.findOne({ id });
            if (!order) return;

            order.status = workOrderStatus.ACKNOWLEDGED;

            await order.save();
            log.info(`Work order ${id} status updated to ACKNOWLEDGED`);
        }
    });

    // Handle work order status update
    host.onRequest(Requests.WORK_ORDER_STATUS_UPDATE, async request => {
        const { ip, data } = request;
        const { id, statusCode } = parseWorkOrderStatus(data);

        log.client(`${request.sourceId} update work order: ${id} to ${workOrderStatus[statusCode]}`);

        // Lookup work order assets
        const { error, assets } = await fetchWorkOrderDataAssets(id);
        if (error || !assets) return;

        // Update order status
        assets.order.status = statusCode;
        await assets.order.save();

        // Handle updated status request
        switch (statusCode) {
            case workOrderStatus.COMPLETE:
                await handleUpdateComplete(assets);
                break;
            case workOrderStatus.PROCESSING:
                await handleUpdateProcessing(assets);
                break;
            case workOrderStatus.CARRIAGE_NOT_EMPTY_TO_PICK:
                await handleUpdateCarriageNotEmpty(assets);
                break;
            case workOrderStatus.NO_ITEM_TO_PLACE:
                await handleUpdateNoItemToPlace(assets);
                break;
            case workOrderStatus.PICK_LOCATION_EMPTY:
                await handleUpdateNoItemToPick(assets);
                break;
            case workOrderStatus.DESTINATION_UNAVAILABLE:
                await handleUpdateDestUnavailable(assets);
                break;
            default:
                log.error(`${ip} sent an unknown work order status ${statusCode}`);
                break;
        }
    });
};
