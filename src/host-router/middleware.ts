import { Host } from '../host';
import { parseResponse } from '../message-util/response';

export const initHostMiddleware = () => {
    // Get useful singletons
    const host = Host.getInstance();

    host.useResponse(response => {
        const { status, statusCode } = parseResponse(response.data);

        response.status = status;
        response.statusCode = statusCode;
    });
};
