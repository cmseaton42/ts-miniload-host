import * as encap from '../src/encapsulation';
import { buildSetTime } from '../src/cmp/set-time';

describe('Packet Encapsulation helper', () => {
    it('Builds packets correctly', () => {
        const packet = encap.Packet.build(Buffer.from('hello'), {
            sourceId: 123456,
            destId: 987654,
        });

        expect(packet).toMatchSnapshot();
    });

    it('Parses packets correctly', () => {
        const packet = encap.Packet.build(Buffer.from('hello'), {
            sourceId: 123456,
            destId: 987654,
        });

        expect(encap.Packet.parse(packet)).toEqual({
            sourceId: 123456,
            destId: 987654,
            data: Buffer.from('hello'),
        });
    });

    it('Validates packets correctly', () => {
        const good = encap.Packet.build(buildSetTime(new Date()), { sourceId: 1337, destId: 7113 });
        expect(encap.Packet.isValidPacket(good)).toBeTruthy();

        let noStx = Buffer.alloc(good.length);
        good.copy(noStx);
        noStx.writeInt8(0, 0);
        expect(encap.Packet.isValidPacket(noStx)).toBeFalsy();

        let noEot = Buffer.alloc(good.length);
        good.copy(noEot);
        noEot.writeInt8(0, noEot.length - 1);
        expect(encap.Packet.isValidPacket(noEot)).toBeFalsy();

        let badSize = Buffer.alloc(good.length);
        good.copy(badSize);
        badSize.writeInt16LE(3, 1);
        expect(encap.Packet.isValidPacket(badSize)).toBeFalsy();

        let badSrc = Buffer.alloc(good.length);
        good.copy(badSrc);
        badSrc.writeInt32LE(-3, 3);
        expect(encap.Packet.isValidPacket(badSrc)).toBeFalsy();

        let badDest = Buffer.alloc(good.length);
        good.copy(badDest);
        badDest.writeInt32LE(-3, 7);
        expect(encap.Packet.isValidPacket(badDest)).toBeFalsy();

        let badReq = Buffer.alloc(good.length);
        good.copy(badReq);
        badReq.writeInt32LE(-3, 11);
        expect(encap.Packet.isValidPacket(badReq)).toBeFalsy();
    });
});
