import {
    buildWorkOrderRequest,
    parseWorkOrderStatus,
    IWorkOrderStatusData,
    IPerformRequestOpts,
} from '../../src/cmp/work-order';
import uuid from 'uuid/v4';

describe('Work order assembly utility functions', () => {
    describe('Perform work order builder', () => {
        it('Throws on bad input', () => {
            const fn = (opts: IPerformRequestOpts) => () => buildWorkOrderRequest(opts);

            const id = uuid();
            expect(fn({ id })).toThrow();
            expect(fn({ id, from: 3, to: 4 })).not.toThrow();
            expect(fn({ id, from: 3 })).not.toThrow();
            expect(fn({ id, to: 4 })).not.toThrow();
            expect(fn({ id: 'not an id', from: 3 })).toThrow();
        });

        it('Produces good output', () => {
            const id = 'f2431e47-a3ca-4ad9-aae4-32c433df47ce';

            let buf = buildWorkOrderRequest({ id, to: 2, from: 45 });
            expect(buf.readInt8(4)).toBe(36);
            expect(buf.readInt32LE(0)).toBe(1250);
            expect(buf.slice(5, 41).toString('utf8')).toBe(id);
            expect(buf.readInt32LE(45)).toBe(45);
            expect(buf.readInt32LE(53)).toBe(2);

            buf = buildWorkOrderRequest({ id, to: 2 });
            expect(buf.readInt32LE(45)).toBe(0);
            expect(buf.readInt32LE(53)).toBe(2);

            buf = buildWorkOrderRequest({ id, from: 45 });
            expect(buf.readInt32LE(45)).toBe(45);
            expect(buf.readInt32LE(53)).toBe(0);
        });
    });

    describe('Parse work order status request', () => {
        it('Produces good output', () => {
            const id = 'f2431e47-a3ca-4ad9-aae4-32c433df47ce';

            const buf = Buffer.alloc(45);
            buf.writeInt32LE(102250, 0);
            buf.writeInt8(36, 4);
            buf.writeInt32LE(1, 41);
            Buffer.from(id).copy(buf, 5, 0);
            expect(parseWorkOrderStatus(buf)).toEqual({ id, status: 'PROCESSING', statusCode: 1 });

            buf.writeInt32LE(20, 41);
            expect(parseWorkOrderStatus(buf)).toEqual({ id, status: '', statusCode: 20 });
        });
    });
});
