import { buildSetTime } from '../../src/cmp/index';

describe('Set time command builder', () => {
    it('Produces the correct output', () => {
        const partyLikeIts1998 = new Date('1998');
        const buf = buildSetTime(partyLikeIts1998);

        expect(buf).toMatchSnapshot();
    });
});
