import { parseResponse } from '../../src/cmp/response';

describe('Response decoder utility', () => {
    it('Parses response packet successfully', () => {
        const buf1 = Buffer.alloc(12);
        buf1.writeInt32LE(1000 + 100000, 0);
        buf1.writeInt32LE(100, 4);
        buf1.writeInt32LE(0, 8);

        expect(parseResponse(buf1)).toEqual({
            id: 1000,
            statusCode: 100,
            status: '',
        });

        const msg = Buffer.from('An error message');
        const buf2 = Buffer.alloc(12 + msg.length);
        buf2.writeInt32LE(1500 + 100000, 0);
        buf2.writeInt32LE(200, 4);
        buf2.writeInt32LE(msg.length, 8);
        msg.copy(buf2, 12, 0);

        expect(parseResponse(buf2)).toEqual({
            id: 1500,
            statusCode: 200,
            status: 'An error message',
        });
    });
});
